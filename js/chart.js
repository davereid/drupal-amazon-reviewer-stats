/**
 * @file
 * Attaches behavior for the Amazon Reviewer Stats module.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Displays the guidelines of the selected text format automatically.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches behavior for updating filter guidelines.
   */
  Drupal.behaviors.amazonReviewerStatsChart = {
    attach: function (context, settings) {

      function drawStatsChart() {
        var data = settings.AmazonReviewerChartData;
        data.unshift([{type: 'date', label: 'Date'}, "Ranking", "Helpful Votes"]);
        data = google.visualization.arrayToDataTable(data);

        /*var options = {
         curveType: 'function',
         series: {
         // Gives each series an axis name that matches the Y-axis below.
         0: {axis: 'Rank'},
         1: {axis: 'Votes'}
         },
         axes: {
         // Adds labels to each axis; they don't have to match the axis names.
         y: {
         Rank: {label: 'Ranking', direction: -1},
         Votes: {label: 'Helpful Votes'}
         }
         }
         };
         var chart = new google.charts.Line(document.getElementById('stats-chart'));*/

        var options = {
          curveType: 'function',
          legend: { position: 'bottom' },
          series: {
            0: {targetAxisIndex: 0},
            1: {targetAxisIndex: 1}
          },
          vAxes: {
            // Adds titles to each axis.
            0: {title: 'Ranking', direction: -1},
            1: {title: 'Helpful Votes'}
          }
        };
        var chart = new google.visualization.LineChart(document.getElementById('stats-chart'));

        chart.draw(data, options);
      }

      google.charts.load('current', {'packages':['corechart','line']});
      google.charts.setOnLoadCallback(drawStatsChart);

    }
  };

})(jQuery, Drupal);
