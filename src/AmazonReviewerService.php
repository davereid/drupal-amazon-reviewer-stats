<?php

namespace Drupal\amazon_reviewer_stats;

use GuzzleHttp\ClientInterface;
use Drupal\Core\Queue\QueueFactory;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AmazonReviewerService.
 *
 * @package Drupal\amazon_reviewer_stats
 */
class AmazonReviewerService implements AmazonReviewerServiceInterface, ContainerInjectionInterface  {

  const FETCH_INTERVAL = 86400;

  /**
   * @var \Drupal\amazon_reviewer_stats\StorageInterface
   */
  protected $storage;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructor.
   */
  public function __construct(StorageInterface $storage, ClientInterface $http_client, QueueFactory $queue_factory) {
    $this->storage = $storage;
    $this->httpClient = $http_client;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('amazon_reviewer_stats.storage'),
      $container->get('http_client'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getProfileUrl($profile_id) {
    return 'https://www.amazon.com/gp/pdp/profile/' . $profile_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function isValidProfileId($profile_id) {
    // Check format first.
    if (!preg_match('/^[A-Z0-9]+$/', $profile_id)) {
      return FALSE;
    }

    // Check that the profile URL has a valid HTTP response.
    $url = static::getProfileUrl($profile_id);
    try {
      \Drupal::httpClient()->request('GET', $url);
    }
    catch (GuzzleException $e) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateStats($profile_id) {
    // If the last stat was fetched less than 24 hours ago, do nothing.
    $last_stat = $this->storage->getLastRecord($profile_id);
    if ($last_stat['timestamp'] > (time() - static::FETCH_INTERVAL)) {
      return $last_stat;
    }

    // Only log if this is the first record, or different from the last statistic.
    $current_stat = $this->fetchStats($profile_id);
    if (!$last_stat || array_diff_assoc($current_stat, $last_stat)) {
      $this->storage->addRecord($profile_id, $current_stat);
    }
    return $current_stat;
  }

  /**
   * {@inheritdoc}
   */
  public function queueUpdates() {
    if ($ids = $this->storage->findProfileIdsNeedUpdating(static::FETCH_INTERVAL)) {
      $queue = $this->queueFactory->get('amazon_reviewer_stats');
      foreach ($ids as $id) {
        $queue->createItem($id);
      }
    }
  }

  protected function fetchStats($profile_id) {
    $url = static::getProfileUrl($profile_id);
    $response = $this->httpClient->request('GET', $url);

    $stats = [];

    $dom = new \DOMDocument();
    @$dom->loadHTML((string) $response->getBody());
    $xpath = new \DOMXPath($dom);

    $ranking = $xpath->query('//span[text()="Reviewer ranking"]/following::*[starts-with(text(),"#")]');
    if (!$ranking->length) {
      $stats['ranking'] = 0;
    }
    else {
      $ranking_value = trim(strtr($ranking->item(0)->nodeValue, array('#' => '', ',' => '')));
      if (!is_numeric($ranking_value)) {
        throw new \RuntimeException("Non-numeric \"Reviewer ranking\" value on {$url}.");
      }
      $stats['ranking'] = intval($ranking_value);
    }

    $helpful = $xpath->query('//span[text()="Helpful votes"]/following::*');
    if (!$helpful->length) {
      $stats['helpful_votes'] = 0;
    }
    else {
      $helpful_value = trim(strtr($helpful->item(0)->nodeValue, array(',' => '')));
      if (!is_numeric($helpful_value)) {
        throw new \RuntimeException("Non-numeric \"Helpful votes\" value on {$url}.");
      }
      $stats['helpful_votes'] = intval($helpful_value);
    }

    return $stats;
  }

}
