<?php

namespace Drupal\amazon_reviewer_stats;

/**
 * Interface AmazonReviewerServiceInterface.
 *
 * @package Drupal\amazon_reviewer_stats
 */
interface AmazonReviewerServiceInterface {

  public static function getProfileUrl($profile_id);

  public function updateStats($profile_id);

  public static function isValidProfileId($profile_id);

  public function queueUpdates();

}
