<?php

namespace Drupal\amazon_reviewer_stats\Controller;

use Drupal\amazon_reviewer_stats\AmazonReviewerServiceInterface;
use Drupal\amazon_reviewer_stats\StorageInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Link;

/**
 * Class AmazonReviewerStatsController.
 *
 * @package Drupal\amazon_reviewer_stats\Controller
 */
class AmazonReviewerStatsController extends ControllerBase {

  /**
   * @var \Drupal\amazon_reviewer_stats\StorageInterface
   */
  protected $storage;

  /**
   * @var \Drupal\amazon_reviewer_stats\AmazonReviewerService
   */
  protected $service;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  public function __construct(StorageInterface $storage, AmazonReviewerServiceInterface $service, DateFormatterInterface $date_formatter) {
    $this->storage = $storage;
    $this->service = $service;
    $this->dateFormatter = $date_formatter;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('amazon_reviewer_stats.storage'),
      $container->get('amazon_reviewer_stats'),
      $container->get('date.formatter')
    );
  }

  public function title($profile_id) {
    return $this->t('Amazon Reviewer Statistics for <a href="@url">@name</a>', ['@name' => $profile_id, '@url' => $this->service->getProfileUrl($profile_id)]);
  }

  private function buildChartingJsData(array $stats) {
    $js_data = [];
    ksort($stats);
    foreach ($stats as $record) {
      $js_data[] = [
        // JavaScript Date() object expects the month parameter to start at 0
        // for Jan, not 1, like PHP outputs.
        sprintf('Date(%d, %d, %d)', gmdate('Y', $record['timestamp']), gmdate('n', $record['timestamp']) - 1, gmdate('j', $record['timestamp'])),
        (int) $record['ranking'],
        (int) $record['helpful_votes'],
      ];
    }
    return $js_data;
  }

  private function buildTableRows(array $stats) {
    $rows = [];
    // Sort the stats to show most recent first.
    krsort($stats);
    // Re-index the stats so we can use a numeric index.
    $stats = array_values($stats);
    foreach ($stats as $index => $record) {
      $rank_diff = 0;
      $votes_diff = 0;
      if ($index < (count($stats) - 1)) {
        $previous = $stats[$index + 1];
        $rank_diff = $previous['ranking'] - $record['ranking'];
        $votes_diff = $record['helpful_votes'] - $previous['helpful_votes'];
        if ($rank_diff > 0) {
          $rank_diff = '<span class="change-positive">+' . $rank_diff . '</span>';
        }
        elseif ($rank_diff < 0) {
          $rank_diff = '<span class="change-negative">' . $rank_diff . '</span>';
        }
        if ($votes_diff > 0) {
          $votes_diff = '<span class="change-positive">+' . $votes_diff . '</span>';
        }
        elseif ($votes_diff < 0) {
          $votes_diff = '<span class="change-negative">' . $votes_diff . '</span>';
        }
      }
      $rows[] = array(
        $this->dateFormatter->format($record['timestamp'], 'custom', 'Y-m-d'),
        '#' . number_format($record['ranking']) . ($rank_diff ? ' (' . $rank_diff . ')' : ''),
        number_format($record['helpful_votes']) . ($votes_diff ? ' (' . $votes_diff . ')' : ''),
      );
    }
    return $rows;
  }

  public function view($profile_id) {
    $this->service->updateStats($profile_id);
    $stats = $this->storage->getAllRecords($profile_id);

    if (!$stats) {
      throw new NotFoundHttpException();
    }

    $build = [];

    $build['chart'] = [
      '#markup' => '<div id="stats-chart"></div>',
      '#attached' => [
        'library' => [
          'amazon_reviewer_stats/chart',
        ],
        'drupalSettings' => [
          'AmazonReviewerChartData' => $this->buildChartingJsData($stats),
        ],
      ],
      '#access' => count($stats) > 1,
    ];

    $build['data'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Date'),
        $this->t('Ranking'),
        $this->t('Helpful Votes'),
      ],
      '#rows' => $this->buildTableRows($stats),
      '#caption' => $this->t('Only dates where the stats changed are shown.'),
    ];

    return $build;
  }

  public function listProfiles() {
    $build = [];

    $profile_rows = array();
    $profiles = db_query("SELECT profile_id, COUNT(timestamp) as timestamp_count FROM {amazon_reviewer_stats} GROUP BY profile_id ORDER BY timestamp")->fetchAllKeyed();
    foreach ($profiles as $id => $count) {
      $last = $this->storage->getLastRecord($id);
      $profile_rows[] = array(
        Link::createFromRoute($id, 'amazon_reviewer_stats.stats_view', ['profile_id' => $id]),
        \Drupal::service('date.formatter')->formatInterval($last['timestamp'] + 86400 - time()),
        $count,
        $last['ranking'] ? '#' . number_format($last['ranking']) : 'N/A',
        number_format($last['helpful_votes']),
      );
    }

    $build['profiles'] = array(
      '#theme' => 'table',
      '#header' => array(
        t('Profile ID'),
        t('Time until next update'),
        t('Records'),
        t('Ranking'),
        t('Helpful Votes'),
      ),
      '#rows' => $profile_rows,
      '#empty' => t('No profiles yet.'),
    );

    return $build;
  }

}
