<?php

namespace Drupal\amazon_reviewer_stats;

use Drupal\Core\Database\Connection;
use PDO;

/**
 * Class Storage.
 *
 * @package Drupal\amazon_reviewer_stats
 */
class DatabaseStorage implements StorageInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing configuration data.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastRecord($profile_id) {
    return $this->connection->queryRange("SELECT timestamp, ranking, helpful_votes FROM {amazon_reviewer_stats} WHERE profile_id = :id ORDER BY timestamp DESC", 0, 1, array(':id' => $profile_id))->fetchAssoc();
  }

  /**
   * {@inheritdoc}
   */
  public function getAllRecords($profile_id) {
    return $this->connection->query("SELECT timestamp, ranking, helpful_votes FROM {amazon_reviewer_stats} WHERE profile_id = :id ORDER BY timestamp", array(':id' => $profile_id))->fetchAllAssoc('timestamp', PDO::FETCH_ASSOC);
  }

  /**
   * {@inheritdoc}
   */
  public function addRecord($profile_id, array $record) {
    $record += [
      'profile_id' => $profile_id,
      'timestamp' => time(),
    ];
    $this->connection->insert('amazon_reviewer_stats')
      ->fields($record)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function findProfileIdsNeedUpdating($interval, $limit = NULL) {
    if ($limit) {
      return $this->connection->queryRange("SELECT profile_id FROM {amazon_reviewer_stats} GROUP BY profile_id HAVING MAX(timestamp) < :timestamp ORDER BY MAX(timestamp)", 0, $limit, array(':timestamp' => time() - $interval))->fetchCol();
    }
    else {
      return $this->connection->query("SELECT profile_id FROM {amazon_reviewer_stats} GROUP BY profile_id HAVING MAX(timestamp) < :timestamp ORDER BY MAX(timestamp)", array(':timestamp' => time() - $interval))->fetchCol();
    }
  }
}
