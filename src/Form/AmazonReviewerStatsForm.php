<?php

namespace Drupal\amazon_reviewer_stats\Form;

use Drupal\amazon_reviewer_stats\AmazonReviewerServiceInterface;
use Drupal\amazon_reviewer_stats\StorageInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AmazonReviewerStatsForm.
 *
 * @package Drupal\amazon_reviewer_stats\Form
 */
class AmazonReviewerStatsForm extends FormBase {


  /**
   * @var \Drupal\amazon_reviewer_stats\StorageInterface
   */
  protected $storage;

  /**
   * @var \Drupal\amazon_reviewer_stats\AmazonReviewerService
   */
  protected $service;

  /**
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  public function __construct(StorageInterface $storage, AmazonReviewerServiceInterface $service, FloodInterface $flood) {
    $this->storage = $storage;
    $this->service = $service;
    $this->flood = $flood;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('amazon_reviewer_stats.storage'),
      $container->get('amazon_reviewer_stats'),
      $container->get('flood')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'amazon_reviewer_stats_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!$this->flood->isAllowed($this->getFormId(), 25)) {
      throw new AccessDeniedHttpException();
    }

    $form['help']['#markup'] = $this->t('To find your Amazon Profile ID, open <a href="@url" target="_blank">@url</a> and copy the ID from the URL that loads.', ['@url' => 'https://www.amazon.com/gp/pdp/profile']);

    $form['profile_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Amazon Profile ID'),
      '#placeholder' => 'A2Q0LCYSV26P8E',
      '#element_validate' => ['amazon_reviewer_stats_element_validate_profile_id'],
      '#required' => TRUE,
      '#size' => 20,
      '#maxlength' => 20,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check stats'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->flood->register($this->getFormId());
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('amazon_reviewer_stats.stats_view', ['profile_id' => $form_state->getValue('profile_id')]);
  }

}
