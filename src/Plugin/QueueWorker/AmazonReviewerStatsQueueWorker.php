<?php

/**
 * @file
 * Contains Drupal\amazon_reviewer_stats\Plugin\QueueWorker\AmazonReviewerStatsQueueWorker.php
 */

namespace Drupal\amazon_reviewer_stats\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\amazon_reviewer_stats\AmazonReviewerServiceInterface;

/**
 * A Node Publisher that publishes nodes on CRON run.
 *
 * @QueueWorker(
 *   id = "amazon_reviewer_stats",
 *   title = @Translation("Amazon Reviewer Stats"),
 *   cron = {
 *     "time" = 60,
 *   }
 * )
 */
class AmazonReviewerStatsQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\amazon_reviewer_stats\AmazonReviewerServiceInterface
   */
  protected $service;

  /**
   * Creates a new AmazonReviewerStatsQueueWorker object.
   */
  public function __construct(AmazonReviewerServiceInterface $service, array $configuration, $plugin_id, $plugin_definition) {
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('amazon_reviewer_stats'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($profile_id) {
    try {
      $this->service->updateStats($profile_id);
    }
    catch (\Exception $e) {
      debug($e->getMessage());
      watchdog_exception('amazon_reviewer_stats', $e);
      // Do not re-throw this exception as we want the queued item to be deleted.
    }
  }
}
