<?php

namespace Drupal\amazon_reviewer_stats;

/**
 * Interface StorageInterface.
 *
 * @package Drupal\amazon_reviewer_stats
 */
interface StorageInterface {

  public function getLastRecord($profile_id);

  public function getAllRecords($profile_id);

  public function addRecord($profile_id, array $record);

  public function findProfileIdsNeedUpdating($interval, $limit = NULL);

}
